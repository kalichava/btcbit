var gulp =          require('gulp'),
    fs =            require('fs'),
    autoprefixer =  require('gulp-autoprefixer'),
    csscomb =       require('gulp-csscomb'),
    cssnano =       require('gulp-cssnano'),
    livereload =    require('gulp-livereload'),
    //postcss =       require('gulp-postcss'),
    sass =          require('gulp-sass'),
    twig =          require('gulp-twig'),

    content =       'src/content.json';

function loadContent(path) {
    return JSON.parse(fs.readFileSync(path));
}

function swallowError (error) {
    console.log(error.toString());
    this.emit('end');
    return false;
}


//
//
// Tasks

gulp.task('sass', function() {
    gulp.src('./src/sass/style.scss')
        .pipe(sass().on('error', swallowError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csscomb())
        .pipe(cssnano())
        .pipe(gulp.dest('./dest/css'))
        .pipe(livereload());
});

gulp.task('twig', function () {
    return gulp.src('./src/html/*.twig')
        .pipe(twig({
            base: './src/html',
            data: loadContent(content),
            filters:  [
                {
                    name: "safeurl",
                    func: function (args) {
                        return args;
                    }
                }
            ]
        }).on('error', swallowError))
        .pipe(gulp.dest('./dest'))
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch(['./src/html/**/*.twig', './src/**/*.json'], ['twig']);
});
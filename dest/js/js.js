$(document).ready(function () {
    $('.dropdownLink a[dropdown-id]').each(function () {
        $(this).qtip({
            style: {
                def: false,
                classes: ''
            },
            content: {
                text: $('#' + $(this).attr('dropdown-id'))
            },
            position: {
                my: 'top center',
                at: 'bottom center',
                adjust: {
                    x: 0,
                    y: 24
                }
            },
            show: {
                solo: true,
                effect: function (offset) {
                    $(this).fadeIn(300);
                }
            },
            hide: {
                fixed: true,
                event: 'unfocus',
                effect: function (offset) {
                    $(this).fadeOut(300);
                }
            }
        });
    });

    var start = new Date;
    var gap = 60;
    var delta = 60;
    setInterval(function () {
        delta = Math.round(gap - ((new Date - start) / 1000));
        if (delta < 1) {
            start = new Date;
            console.log("once again");
        }
        $('#countdownTimer').text(
            delta + " seconds");
    }, 1000);


    $('.mainMenuLink').click(function () {
        $(this).toggleClass('is-active');
        $('.mobileMenu').toggleClass('active');
    });


    $('#tab-container').easytabs();
    $('#tab-inner-container').easytabs();
    $('#tab-inner-container-2').easytabs();
    $('#tab-inner-container-3').easytabs();


    $('.faq .faq__item').click(function () {
        $(this).toggleClass('active');
    });


    window.currentTab = 'charts';
    // console.log(window.currentTab);


    var currenciesEl = $('.section.currencies');
    var marketEl = $('.section.market');
    var historyEl = $('.section.tradeHistory');
    var buySellEl = $('.wrapper--buy-sell');
    var chartEl = $('.section.section--chart');
    var transactionsEl = $('.section.latestTransactions');

    var centerCol = $('.main__center');
    var leftCol = $('.main__left');
    var rightCol = $('.main__right');



    function filterDashboard() {
        console.log('in filterDashboard: ' + window.currentTab);
        $('[data-family]').hide();
        $('[data-family]').each(
            function () {
                var families = $(this).attr('data-family').split(',');
                if (families.indexOf(window.currentTab) > -1) {
                    $(this).show();
                }
            }
        );
    }

    function moveSection(sm) {
        console.log('in moveSection: ' + window.currentTab);
        if (sm.matches) {
            filterDashboard();
            chartEl.detach().prependTo(leftCol);
            transactionsEl.detach().prependTo(leftCol);
            buySellEl.detach().prependTo(leftCol);

        } else {
            $('[data-family]').show();
            // window.currentTab = false;
            chartEl.detach().prependTo(centerCol);
            transactionsEl.detach().prependTo(rightCol);
            buySellEl.detach().appendTo(centerCol);

        }
        changeTabs();
    }

    function changeTabs() {
        // console.log(window.currentTab);
        $('.tabs--dashboard li').removeClass('active');
        $(`.tabs--dashboard li a[data-filter="${window.currentTab}"]`).parent().addClass('active');
    }

    $('.tabs--dashboard li a').click(function (e) {
        e.preventDefault();
        window.currentTab = $(e.target).attr('data-filter');
        changeTabs();
        filterDashboard();
    });


    // pickadate
    $('.datepicker--from').pickadate({
        today: '',
        clear: '',
        close: ''
    });
    $('.datepicker--to').pickadate({
        today: '',
        clear: '',
        close: '',
        onOpen: function () {
            $(this.$node).next('.picker').css('right', 0);
        },
    });

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).html()).select();
        document.execCommand("copy");
        $temp.remove();
    }

    $('.linkToCopy').click(function () {
        copyToClipboard($(this).children('.hidden'));
        $(this).append('<span class="copied"><i class="mdi mdi-check add8right"></i>Copied</span>');
        console.log('copied');
    });

    $('.readMoreLink').click(function () {
        $(this).hide();
        $('.readMoreContent').show();
    });

    var sm = window.matchMedia("(max-width: 1024px)");
    var xs = window.matchMedia("(max-width: 763px)");




    function moveNote(xs) {
        if (xs.matches) {
            
            targetNote.detach().appendTo(destinationNoteParent);
            targetNote2.detach().appendTo(destinationNoteParent2);
        } else {
            targetNote.detach().appendTo(targetNoteParent);
            targetNote2.detach().appendTo(targetNoteParent2);
        }
    }
    var targetNote = $('.note--trade');
    var targetNoteParent = $('.note--trade').parent();
    var destinationNote = $('.note--get');
    var destinationNoteParent = $('.note--get').parent();
    var targetNote2 = $('.note--trade2');
    var targetNoteParent2 = $('.note--trade2').parent();
    var destinationNote2 = $('.note--get2');
    var destinationNoteParent2 = $('.note--get2').parent();



    moveSection(sm);
    sm.addListener(moveSection);

    moveNote(xs);
    xs.addListener(moveNote);

});
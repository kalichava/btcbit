sass_dir = "sass"

http_path = "../dest"
css_dir = "../dest/css"
images_dir = "../dest/img"
javascripts_dir = "../dest/js"

sass_optoins = {
    :filename => 'style.css'
}

output_style = :compressed
line_comments = false

# output_style = :expanded or :nested or :compact or :compressed
# relative_assets = true

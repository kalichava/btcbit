
import React from "react";
import PropTypes from "prop-types";

import { scaleTime } from "d3-scale";
import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import {
	BarSeries,
	CandlestickSeries,
} from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import {PriceCoordinate} from "react-stockcharts/lib/coordinates";
import {
	CrossHairCursor,
	EdgeIndicator,
	CurrentCoordinate,
	MouseCoordinateX,
	MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";

import { HoverTooltip } from "react-stockcharts/lib/tooltip";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last, hexToRGBA } from "react-stockcharts/lib/utils";
import { Transform } from "stream";

import { ema } from "react-stockcharts/lib/indicator";

const dateFormat = timeFormat("%B %d, %Y");
const numberFormat = format(".2f");

function tooltipContent(ys) {
	return ({ currentItem, xAccessor }) => {
		return {
			x: dateFormat(xAccessor(currentItem)),
			y: [
				{
					label: false,
					value: currentItem.close && numberFormat(currentItem.close)
				}
			]
				.concat(
					ys.map(each => ({
						label: each.label,
						value: each.value(currentItem),
						stroke: 0
					}))
				)
				.filter(line => line.value)
		};
	};
}

class CandleStickChart extends React.Component {
	render() {
		const { type, data, width, ratio } = this.props;
		const margin = { left: 0, right: 0, top: 0, bottom: 0 };

		const xAccessor = d => d.date;
		const start = xAccessor(last(data));
		const end = xAccessor(data[Math.max(0, data.length - 200)]);
		const xExtents = [start, end];
		const { gridProps, seriesType } = this.props;

		const height = 500;
		const gridHeight = height - margin.top - margin.bottom;
		// const gridWidth = width - margin.left - margin.right;
		const gridWidth = width;


		const showGrid = true;
		const yGrid = showGrid ? { innerTickSize: -1 * gridWidth } : {};
		const xGrid = showGrid ? { innerTickSize: -1 * gridHeight } : {};


		const candlesAppearance = {
			wickStroke: function fill(d) {
				return d.close > d.open ? "rgba(229, 85, 65, 1)" : "rgba(7, 216, 94, 1)";
			},
			candleStrokeWidth: 0,
			fill: function fill(d) {
			  return d.close > d.open ? "rgba(229, 85, 65, 1)" : "rgba(7, 216, 94, 1)";
			},
			width: 5,
			// stroke: "rgba(53, 64, 82, 1)",
			opacity: 1
		  }

		const ema50 = ema()
			.id(2)
			.options({ windowSize: 50 })
			.merge((d, c) => {
				d.ema50 = c;
			})
			.accessor(d => d.ema50);


		return (
			<ChartCanvas height={500}
					ratio={ratio}
					width={width}
					margin={{ left: 0, right: 20, top: 10, bottom: 30 }}
					type={type}
					seriesName="MSFT"
					data={data}
					xScale={scaleTime()}
					xAccessor={xAccessor}
					xExtents={xExtents}>
				<Chart id={1}
						yExtents={[d => [d.high, d.low]]}
						padding={{ top: 40, bottom: 20 }}>
				<XAxis
					axisAt="bottom"
					orient="bottom"
					{...gridProps}
					{...xGrid}
				/>
				<YAxis
					axisAt="right"
					orient="right"
					ticks={5}
					{...gridProps}
					{...yGrid}
				/>

				<CandlestickSeries {...candlesAppearance} />

				{/* <HoverTooltip
					yAccessor={ema50.accessor()}
					tooltipContent={tooltipContent([])}
					fontSize={12}
					bgFill="rgba(0,0,0,0)"
					bgOpacity={0.5}
					stroke="rgba(0,0,0,0)"
					fontFill="#fff"
				/> */}

				<PriceCoordinate
					at="right"
					orient="right"
					price={60}
					lineStroke="rgba(7, 216, 94, 1)"
					lineOpacity={1}
					textFill="transparent"
					fill="transparent"
					strokeDasharray="ShortDash"
					displayFormat={format(".2f")}
				/>

				<PriceCoordinate
					at="right"
					orient="right"
					price={55}
					lineOpacity={1}
					lineStroke="rgba(229, 85, 65, 1)"
					textFill="transparent"
					fill="transparent"
					strokeDasharray="ShortDash"
					displayFormat={format(".2f")}
				/>

					
			</Chart>
			<CrossHairCursor />
		</ChartCanvas>
		);
	}
}

CandleStickChart.propTypes = {
	data: PropTypes.array.isRequired,
	width: PropTypes.number.isRequired,
	ratio: PropTypes.number.isRequired,
	type: PropTypes.oneOf(["svg", "hybrid"]).isRequired,
};

CandleStickChart.defaultProps = {
	type: "svg",
};
CandleStickChart = fitWidth(CandleStickChart);

export default CandleStickChart;